#include <Usb.h>
#include <usbhub.h>
#include <usbh_midi.h>
#include <SoftPWM.h>

USB Usb;
USBH_MIDI Midi(&Usb);

// Effect pins and settings
int inPin1 = 4; // Effect one
int inPin2 = 6; // Effect two
int inPin3 = 8; // Effect three
int reading1;
int reading2;
int reading3;
int previous1 = LOW;
int previous2 = LOW;
int previous3 = LOW;

// LED pins and settings
int outPin1 = 14; // LED Effect one
int outPin2 = 15; // LED Effect two
int outPin3 = 16; // LED Effect three
int state1 = LOW;
int state2 = LOW;
int state3 = LOW;

long time = 0;         // the last time the output pin was toggled
long debounce = 200;   // the debounce time, increase if the output flickers

void setup() {
  // For Debugging
  Serial.begin(9600);
  Serial.print("Setup..\n");

  Serial.println("Waiting for device to start..");
  delay(5000);

  // Initialize SoftPWM to use Analog pins as digital ones
  SoftPWMBegin();

  // Set Pin modes
  pinMode(inPin1, INPUT);
  pinMode(inPin2, INPUT);
  pinMode(inPin3, INPUT);
  pinMode(outPin1, OUTPUT);
  pinMode(outPin2, OUTPUT);
  pinMode(outPin3, OUTPUT);

  // Setup Shield
  pinMode(7, OUTPUT);  
  digitalWrite(7, HIGH); 

  // Initialize USB
  if (Usb.Init() == -1)
  {
    Serial.println("Error initializing USB..");
    while (1);              // Halt
  }

  // Send Midi 10 times (Otherwise it doesn't freaking work)
  for (int i = 0; i < 10; i++) {
    SendMIDI(0x00);
    delay(200);
  }

  // Patch used for the three effects
  SendMIDI(0x31);                               

  // Enable parameter edit mode on the ZOOM to send SysEx 
  SendParameterEdit(); 

  // Flash sequence for LEDs to show that the Arduino is ready
  for (int i = 0; i < 4; i++) {
    digitalWrite(outPin1, HIGH);
    digitalWrite(outPin2, HIGH);
    digitalWrite(outPin3, HIGH);
    delay(300);
    digitalWrite(outPin1, LOW);
    digitalWrite(outPin2, LOW);
    digitalWrite(outPin3, LOW);
    delay(200);
  }
}

void loop() {
  reading1 = digitalRead(inPin1);
  reading2 = digitalRead(inPin2);
  reading3 = digitalRead(inPin3);

  {
    if (reading1 == HIGH && previous1 == LOW && millis() - time > debounce) {
      
      if (state1 == LOW)
      {
        // Turn effect 1 on
        SendSysex(true, 0x00);
        state1 = HIGH;
      }
      else
      {
        // Turn effect 1 off
        SendSysex(false, 0x00);
        state1 = LOW;
      }
      time = millis();
    }
    digitalWrite(outPin1, state1);
    previous1 = reading1;
  }
  {
    if (reading2 == HIGH && previous2 == LOW && millis() - time > debounce) {
      
      if (state2 == LOW)
      {
        // Turn effect 2 on
        SendSysex(true, 0x01);
        state2 = HIGH;
      }
      else
      {
        // Turn effect 2 off
        SendSysex(false, 0x01);
        state2 = LOW;
      }
      time = millis();
    }
    digitalWrite(outPin2, state2);
    previous2 = reading2;
  }
  {
    if (reading3 == HIGH && previous3 == LOW && millis() - time > debounce) {
      
      if (state3 == LOW)
      {
        // Turn effect 3 on
        SendSysex(true, 0x02);
        state3 = HIGH;
      }
      else
      {
        // Turn effect 3 off
        SendSysex(false, 0x02);
        state3 = LOW;
      }
      time = millis();
    }
    digitalWrite(outPin3, state3);
    previous3 = reading3;
  }
}

// Send "Program Change" MIDI Message (Used for changing presets)
void SendMIDI(byte number)
{
  Usb.Task();
  if( Usb.getUsbTaskState() == USB_STATE_RUNNING )
  {   
    byte Message[2];                 // Construct the midi message
    Message[0]=0xC0;                 // 0xC0 is for Program Change 
    Message[1]=number;               // Number is the program/patch 
    Midi.SendData(Message);          // Send the message
    delay(10);
  }
}

// Send Sysex messages (Used for changing parameters)
void SendSysex(boolean isEnabled, byte effect)
{
  Usb.Task();
  if ( Usb.getUsbTaskState() == USB_STATE_RUNNING )
  {
    byte Message[10];                             // Construct the midi message
    Message[0] = 0xF0;                            // Begin Sysex
    Message[1] = 0x52;                            // Manufacturing ID for ZOOM
    Message[2] = 0x00;                            // Device ID
    Message[3] = 0x58;                            // Model number of MS50g
    Message[4] = 0x31;                            // Command
    Message[5] = effect;                          // Effect number (0x00 - 0x03), the other 3 have to be changed in another way, see Readme.md
    Message[6] = 0x00;                            // Command
    if (isEnabled) {
      Message[7] = 0x01;                          // Effect on 
    }
    else 
    {
      Message[7] = 0x00;                          // Effect off
    }
    Message[8] = 0x00;                            // Command
    Message[9] = 0xF7;                            // End Sysex
    Midi.SendSysEx(Message, sizeof(Message));     // Send the message
    delay(10);
  }
  else
  {
    Serial.println("Error sending SysEx over USB..");
  }
}

// Enable parameter mode on the Zoom, is required to send Sysex
void SendParameterEdit()
{
  Serial.println("Activating Parameter Edit..");
  Usb.Task();
  if ( Usb.getUsbTaskState() == USB_STATE_RUNNING )
  {
    byte Message[10];                             // Construct the midi message
    Message[0] = 0xF0;                            // Begin Sysex
    Message[1] = 0x52;                            // Manufacturing ID for ZOOM
    Message[2] = 0x00;                            // Device ID
    Message[3] = 0x58;                            // Model number of MS50g
    Message[4] = 0x50;                            // Command
    Message[5] = 0xF7;                            // Command
    Midi.SendSysEx(Message, sizeof(Message));     // Send the message
    delay(10);
    Serial.println("-> Success!");
  }
  else
  {
    Serial.println("-> Error!");
  }
}  
