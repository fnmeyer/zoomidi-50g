# ZooMidi-50G

A USB to Midi implementation using an Arduino UNO and the Zoom-MS50G. This work is based on the implementation of Lawrence Doss which you can find [here](http://lawrencedoss.blogspot.com/2014/11/my-tryst-with-hacking-zoom-ms-70cdr.html).

I don't really want to change Presets because i wanted to have three individual pedals in one. So i use SysEx messages to turn on effect 1 - 3 individually. This also works with effect 4 - 6 when you send complete presets. 
This was a bit too much so i decided to just use the first three effects. The box i used was a Xbox 360 power supply, which was perfect for my use and it even had some nearly perfect cutouts for the Arduino ports. 

## Scheme

The finished work i use doesn't apply to this scheme as you can see in the intro text above. It's nearly the same for my usecase. When i have some spare time i will make another scheme for my current work.

![alt text](https://gitlab.com/Fischer96/zoomidi-50g/raw/master/scheme.png "Scheme")

 Credits: [Lawrence Doss](http://lawrencedoss.blogspot.com/2014/11/my-tryst-with-hacking-zoom-ms-70cdr.html)

## Parts

- Arduino UNO
- Noname USB Host Shield like [this](https://store-cdn.arduino.cc/uni/catalog/product/cache/1/image/500x375/f8876a31b63532bbba4e781c30024a0a/a/0/a000004_iso_2.jpg)
- 3x momentary footswitches (SPST)
- 3x LED with holders
- 3x 320 Ohm and 3x 10k Ohm Resistors
- Wires
- USB-B to USB-A cable for code edits on the arduino
- USB-A to Mini-USB for the connection from the Host Shield to the Zoom-MS50G
- A 9V DC power supply for the Arduino (Center positive!)
- Circuit board
- A box of choice

## Images

<img src="https://i.imgur.com/7dK3k5v.jpg"  width="300" height="300">

<img src="https://i.imgur.com/8v4jXnP.jpg"  width="300" height="300">

<img src="https://i.imgur.com/eALxsKz.jpg"  width="300" height="300">

## Possible changes you could make to the switches

- Changing Presets
- Use one switch as tuner
- Use one switch for tap tempo (Not sure about this one)
- Change specific knobs for a effect (Like delay time in 100ms steps or something like that)